
from gi.repository import Gtk,  Gdk, GdkPixbuf, GObject
import cairo

@Gtk.Template(resource_path='/org/gnome/Gallery/ui/image_view_page.ui')
class ImageViewPage(Gtk.Box):

    __gtype_name__ = 'ImageViewPage'

    __gsignals__ = {'prev_image_request' : (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, ()),
                    'next_image_request' : (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, ())  }

    drawing_area = Gtk.Template.Child()
    prev_revealer = Gtk.Template.Child()
    next_revealer = Gtk.Template.Child()
    prev_button = Gtk.Template.Child()
    next_button = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

        # setup tranlation
        self.input_x = 0
        self.input_y = 0
        self.zoom_x = 0
        self.zoom_y = 0
        self.off_x = 0
        self.off_y = 0
        self.delta_off_x = 0
        self.delta_off_y = 0
        self.scale = 1

        # assign style class to osd buttons
        self.prev_button.add_css_class("osd")
        self.next_button.add_css_class("osd")

        # setup gestures
        self.zoom_gesture = Gtk.GestureZoom.new()
        self.zoom_gesture.set_propagation_phase(Gtk.PropagationPhase.BUBBLE)
        self.zoom_gesture.connect("begin", self.zoom_begin)
        self.zoom_gesture.connect("scale-changed", self.zoomed)
        self.drawing_area.add_controller(self.zoom_gesture)

        '''
        press_gesture = Gtk.GestureLongPress.new()
        press_gesture.set_propagation_phase(Gtk.PropagationPhase.BUBBLE)
        press_gesture.connect("pressed", self.pressed)
        self.drawing_area.add_controller(press_gesture)

        swipe_gesture = Gtk.GestureSwipe.new()
        swipe_gesture.set_propagation_phase(Gtk.PropagationPhase.BUBBLE)
        #swipe_gesture.connect("swipe", self.swiped)
        self.drawing_area.add_controller(swipe_gesture)
        '''

        drag_gesture = Gtk.GestureDrag.new()
        drag_gesture.set_propagation_phase(Gtk.PropagationPhase.BUBBLE)
        #drag_gesture.connect("drag-begin", self.drag_begin)
        drag_gesture.connect("drag-update", self.drag)
        drag_gesture.connect("drag-end", self.drag_end)
        self.drawing_area.add_controller(drag_gesture)

        # setup mouse input handling
        click_gesture = Gtk.GestureClick.new()
        click_gesture.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
        click_gesture.connect("pressed", self.click)
        self.drawing_area.add_controller(click_gesture)

        scroll_event = Gtk.EventControllerScroll.new(Gtk.EventControllerScrollFlags.VERTICAL)
        #scroll_event.connect("scroll-begin", self.scroll_begin)
        scroll_event.connect("scroll", self.scroll)
        self.drawing_area.add_controller(scroll_event)

        motion_event = Gtk.EventControllerMotion.new()
        motion_event.connect("motion", self.motion)
        self.drawing_area.add_controller(motion_event)

        self.drawing_area.set_draw_func(self.on_draw)

    def set_image(self, image_path):
        self.reset()
        self.image = GdkPixbuf.Pixbuf.new_from_file(image_path)

    @Gtk.Template.Callback()
    def prev_image_pressed(self, sender):
        #print("prev image")
        self.emit("prev_image_request")

    @Gtk.Template.Callback()
    def next_image_pressed(self, sender):
        #print("next image")
        self.emit("next_image_request")

    def zoom_begin(self, sender, x):

        cen = self.zoom_gesture.get_bounding_box_center()
        #print("zoom begin:", cen)
        if cen[0]:
            self.input_x = cen.x
            self.input_y = cen.y

    def zoomed(self, sender, scale):
        #print("zoomed", scale)
        delta = scale - 1
        self.zoom(delta)

    '''
    def swiped(self, sender, three, four):
        print("swiped")


    def drag_begin(self, sender, x, y):
        print("drag begin", x, y)
    '''

    def drag(self, sender, off_x, off_y):
        #print("dragged", off_x, off_y)
        if self.scale > 1:
            #print("offset:", self.off_x, self.off_y)
            self.delta_off_x = off_x
            self.delta_off_y = off_y
            self.drawing_area.queue_draw()

    def drag_end(self, sender, off_x, off_y):
        #print("drag_end")
        #print("offset:", off_x, off_y)
        self.delta_off_x = 0
        self.delta_off_y = 0
        if self.scale > 1:
            self.off_x += off_x
            self.off_y += off_y
            self.drawing_area.queue_draw()

    def motion(self, sender, x, y):
        #print("motion", x, y)
        self.input_x = x
        self.input_y = y

    '''
    def pressed(self, sender, x, y):
        print("pressed", x, y)
    '''

    def click(self, sender, count, x, y):
        #print("mouse clicked - count:", count)

        if count == 1:
            if self.scale == 1:
                self.toggle_controls()

        elif count > 1:
            self.reset()

    def toggle_controls(self):
        show = not self.prev_revealer.get_reveal_child()
        self.reveal_controls(show)

    def reveal_controls(self, show):
        self.prev_revealer.set_reveal_child(show)
        self.next_revealer.set_reveal_child(show)

    '''
    def scroll_begin(self, sender, x, y):
        print("scroll_begin:", x, y)
    '''

    def scroll(self, sender, horizonal, vertical):
        #print("scroll:", sender, horizonal, vertical)
        self.zoom(vertical)

    def zoom(self, delta):
        #print("zoom:", delta)
        self.scale += delta * 0.1
        if self.scale < 1:
            #self.scale = 1
            self.reset()
        elif self.scale > 2:
            self.scale = 2

        self.zoom_x = self.input_x
        self.zoom_y = self.input_y

        self.drawing_area.queue_draw()

    def reset(self):
        #print("reset")
        self.input_x = 0
        self.input_y = 0
        self.off_x = 0
        self.off_y = 0
        self.delta_off_x = 0
        self.delta_off_y = 0
        self.scale = 1
        self.reveal_controls(True)
        self.drawing_area.queue_draw()

    def get_scale_to_fit(self):

        area_rect = self.drawing_area.get_allocation()
        img_width = float(self.image.get_width())
        img_height = float(self.image.get_height())
        width_ratio = area_rect.width / img_width
        height_ratio = area_rect.height / img_height

        scale = min(width_ratio, height_ratio)

        off_x = (area_rect.width - round(img_width * scale)) * 0.5
        off_y = (area_rect.height - round(img_height * scale)) * 0.5

        return scale, off_x, off_y


    def on_draw(self, sender, context, four, five):

        scale_to_fit, off_x, off_y = self.get_scale_to_fit()
        context.save()

        # scale and translate the image to fit centre on screen
        if scale_to_fit:
            context.translate(off_x, off_y)
            context.scale(scale_to_fit, scale_to_fit)

        # handle user scale and tranlations
        if self.scale > 1:
            self.reveal_controls(False)

            # Convert coordinates to canvas coordinates
            x = self.zoom_x / scale_to_fit - off_x / scale_to_fit
            y = self.zoom_y / scale_to_fit - off_y / scale_to_fit

            # zoom at mouse
            # translate selected zoom point to 0,0 zoom and revert translation
            context.translate(x, y)
            context.scale(self.scale, self.scale)
            context.translate(-x, -y)

            # pan
            total_scale = scale_to_fit * self.scale
            context.translate(self.off_x / total_scale + self.delta_off_x / total_scale,
                              self.off_y / total_scale + self.delta_off_y / total_scale)

        # draw the selected image
        Gdk.cairo_set_source_pixbuf(context, self.image, 0, 0)

        context.paint()
        context.restore()
