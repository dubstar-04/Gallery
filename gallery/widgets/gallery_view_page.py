
from gi.repository import Gtk, Gio, GObject
import os
from . import image_widget

@Gtk.Template(resource_path='/org/gnome/Gallery/ui/gallery_view_page.ui')
class GalleryViewPage(Gtk.ScrolledWindow):

    __gtype_name__ = 'GalleryViewPage'

    __gsignals__ = {'image_selected' : (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, ()) }

    grid_layout = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

        home = os.path.expanduser('~')
        pictures = os.path.join(home, "Pictures")

        self.model_items = Gtk.DirectoryList(attributes="standard::*")
        self.model_items.set_file(Gio.File.new_for_path(pictures))
        self.filter = Gtk.FileFilter()
        supported_types = ["png", "jpg", "jpeg"]
        for file_type in supported_types:
            self.filter.add_suffix(file_type)

        self.filter_list = Gtk.FilterListModel()
        self.filter_list.set_model(self.model_items)
        self.filter_list.set_filter(self.filter)
        self.filter_list.set_incremental(True)

        self.list_model = Gtk.SingleSelection(model=self.filter_list)
        self.list_model.set_autoselect(False)
        self.list_model.connect('selection-changed', self.selection_changed)

        self.factory = Gtk.SignalListItemFactory()
        self.factory.connect('setup', self.on_factory_setup)
        self.factory.connect('bind', self.on_factory_bind)
        self.factory.connect('unbind', self.on_factory_unbind)
        self.factory.connect('teardown', self.on_factory_teardown)

        self.grid_layout.set_model(self.list_model)
        self.grid_layout.set_factory(self.factory)

    def on_factory_setup(self, widget, item: Gtk.ListItem):
        #iw = Gtk.Image()
        iw = image_widget.ImageWidget()
        grid_width = self.grid_layout.get_allocated_width()
        iw.set_pixel_size((grid_width / 2) * 0.95 )
        item.set_child(iw)

    def on_factory_bind(self, widget: Gtk.ListView, item: Gtk.ListItem):
        data = item.get_item()
        gfile = data.get_attribute_object("standard::file")
        image = item.get_child()
        image.set_from_file(gfile.get_path())

    def on_factory_unbind(self, widget, item: Gtk.ListItem):
        pass

    def on_factory_teardown(self, widget, item: Gtk.ListItem):
        pass

    def get_selected_file(self):
        print("get selected")
        selected = self.list_model.get_selected_item()
        gfile = selected.get_attribute_object("standard::file")
        return gfile.get_path()

    def update_selection(self, change: int):
        selected_index = self.list_model.get_selected()
        count = self.model_items.get_n_items()

        next_index = selected_index + change
        if  next_index < count:
            self.list_model.set_selected(next_index)


    def selection_changed(self, sel: Gtk.SingleSelection, index, qty):
        print("image selected index:", index)
        self.emit("image_selected")



